class ExpiredAccountsController < ApplicationController
  before_filter :authenticate_user!, except: [:new]

  def new
    @usuario = nil
    if params.present? && !params[:email].nil?
      @usname= params[:email]
      @usem = params[:email]
      @usuario = User.where("user_name = ? OR email=?", @usname, @usem)
      if !User.find_by_email(params[:email]).nil? || !User.find_by_user_name(params[:email]).nil?
        @usuario.each do |user|
          fecha = nil
          mes = nil

          #mes = 1.month.ago
          #fecha = (Time.now - users.last_activity_at)

          if user.last_activity_at.nil?
            @last = 2.month.ago
          else
            @last = last_activity_at
          end
          if @last < 1.month.ago
            @usr_name = user.name
            @usr_last = user.last_name
            @usr_mail = user.email
            @usr_area = user.area_id
            @usr_flag = user.profile.flag


            # users.last_activity_at = Time.now
            # users.expired_at = nil
            # users.save
            # Expired.avisar_activacion(users).deliver

            if @usr_flag == 1
              @admina = Profile.where("flag = 0")
              @admina.each do |pfid|
                @admin = User.where("profile_id = ?", pfid.id)
                @admin.each do |padmin|
                  @email = padmin.email

                  @usr= User.find_by_email(@usr_mail)
                  @usr.activo = 1
                  @usr.save
                  Expired.avisar_activacion(@email, user).deliver
                end
              end
              redirect_to new_user_session_path, :notice => t('all.we_activation')
            end

            if @usr_flag >= 2
              @admina = Profile.where("flag = 1 and id_area = ?", @usr_area)
              @admina.each do |pfid|
                @admin = User.where("profile_id = ?", pfid.id)
                @admin.each do |padmin|
                  @email = padmin.email

                  @usr= User.find_by_email(@usr_mail)
                  @usr.activo = 1
                  @usr.save
                  Expired.avisar_activacion(@email, user).deliver
                end
              end
              redirect_to new_user_session_path, :notice => t('all.we_activation')
            end
          else
            redirect_to expired_accounts_new_path, :notice => t('all.your_activation')
          end
        end
      else
        redirect_to expired_accounts_new_path, :alert => t('all.no_incoming')
      end
    else
    end
  end
end
