# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# ##################### VISTAS BASE ######################
# vista = View.new
# vista.name = "Home"
# vista.crear = 0
# vista.editar = 0
# vista.eliminar = 0
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "Users"
# vista.crear = 0
# vista.editar = 1
# vista.eliminar = 1
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "Profiles"
# vista.crear = 1
# vista.editar = 1
# vista.eliminar = 1
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "Areas"
# vista.crear = 1
# vista.editar = 1
# vista.eliminar = 1
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "Views"
# vista.crear = 1
# vista.editar = 1
# vista.eliminar = 1
# vista.leer = 1
# vista.save
#
#
#
# ##################### AREA CENTRAL ######################
# area = Area.new
# area.name = "Central"
# area.save
#
# ##################### PERFIL ADMINISTRADOR CENTRAL ######################
# perfil = Profile.new
# perfil.name = 'Administrator Central'
# perfil.area_id = 1
# perfil.flag = 0
# perfil.save
#
# permiso = Permission.new
# permiso.view_id = 1
# permiso.view_name = View.find(1).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = 2
# permiso.eliminar = nil
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 2
# permiso.view_name = View.find(2).name
# permiso.crear = nil
# permiso.editar = 4
# permiso.leer = 2
# permiso.eliminar = 1
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 3
# permiso.view_name = View.find(3).name
# permiso.crear = 8
# permiso.editar = 4
# permiso.leer = 2
# permiso.eliminar = 1
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 4
# permiso.view_name = View.find(4).name
# permiso.crear = 8
# permiso.editar = 4
# permiso.leer = 2
# permiso.eliminar = 1
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 5
# permiso.view_name = View.find(5).name
# permiso.crear = 8
# permiso.editar = 4
# permiso.leer = 2
# permiso.eliminar = 1
# permiso.profile_id = 1
# permiso.save
#
# ##################### PERFIL Default CENTRAL ######################
# perfil = Profile.new
# perfil.name = 'Default Central'
# perfil.area_id = 1
# perfil.flag = 1
# perfil.save
#
# permiso = Permission.new
# permiso.view_id = 1
# permiso.view_name = View.find(1).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = 2
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 2
# permiso.view_name = View.find(2).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = nil
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 3
# permiso.view_name = View.find(3).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = nil
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 4
# permiso.view_name = View.find(4).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = nil
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 5
# permiso.view_name = View.find(5).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = nil
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save